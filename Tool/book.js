const path = require('path');
const fs = require('fs');
const https = require('https');

const textMapFolderPath = path.resolve('../Resources/TextMap');
const textMapFiles = fs.readdirSync(textMapFolderPath);

const getHashEN = readJsonFile('../Resources/TextMap/TextMapEN.json');
const getHashCN = readJsonFile('../Resources/TextMap/TextMapCHS.json'); // beta stuff

// TODO: API IMAGE
// https://rerollcdn.com/STARRAIL/Characters/Thumb/${id}.png
// https://api.yatta.top/hsr/assets/UI/avatar/medium/${id}.png
// https://act-webstatic.hoyoverse.com/game_record/hkrpg/custom/avatar_image/${id}@2x.png
// https://api.yatta.top/hsr/assets/UI/avatar/round/${id}.png (small avatar)
// https://api.yatta.top/hsr/assets/UI/item/
// AssetStudio.CLI.exe "F:\Game\SR\Star Rail game\StarRail_Data\StreamingAssets\Asb\Windows" "C:\Users\siakb\Desktop\Project\ASS\MELON\sr\25" --silent --game "SR" --types "Texture2D" --types "Sprite" --group_assets "ByContainer"

async function runAvatar(lang, getHashLANG, dlicon = false) {
    const saveAvatar = lang + `avatar.json`;
    const getAvatar = readJsonFile('../Resources/ExcelOutput/AvatarConfig.json');
    var dataAvatar = [];
    for (const data of Object.values(getAvatar)) {

        if (data && data.AvatarName && data.AvatarName.Hash) {
            const hash = data.AvatarName.Hash;
            const id = data.AvatarID;
            const rank = data.Rarity;

            //console.log(hash)

            const name = getHashLANG[hash] || getHashCN[hash] || `N/A`;

            var urlIcon = ""
            if (dlicon) {
                //var iconUrl = `https://enka.network/ui/hsr/SpriteOutput/AvatarRoundIcon/${id}.png`;
                try {
                    urlIcon = await downloadIcon(
                        `../Tool/dump/spriteoutput/avatarroundicon/avatar/${id}.png`,
                        `../Tool/icon/avatar/${id}.png`,
                        `/image/game/starrails/avatar/${id}.png`)
                } catch (error) {
                    console.log(error)
                }
            }

            var objAvatar = new Object();
            objAvatar["id"] = id;
            objAvatar["name"] = name;
            objAvatar['img'] = urlIcon;
            objAvatar['rank'] = rank;

            dataAvatar.push(objAvatar)

        } else {
            console.log("skip", data);
        }

    }
    fs.writeFileSync(saveAvatar, JSON.stringify(dataAvatar, null, 2));
    dataAvatar = []; // cleanup
    console.log(`done avatar: ` + saveAvatar)
}
function runScene(lang, getHashLANG) {
    const saveScene = lang + `scene.json`;
    const getScene = readJsonFile('../Resources/ExcelOutput/MazePlane.json');
    var dataScene = [];
    for (const data of Object.values(getScene)) {

        if (data && data.PlaneName && data.PlaneName.Hash) {
            const hash = data.PlaneName.Hash;
            const id = data.PlaneID;

            const name = getHashLANG[hash] || getHashCN[hash] || `N/A`;

            var objScene = new Object();
            objScene["id"] = id;
            objScene["name"] = `${name} (${data.PlaneType})`;
            objScene["floor_default"] = data.StartFloorID;
            objScene["floor"] = data.FloorIDList; // TODO: get better name
            dataScene.push(objScene)

        } else {
            console.log("skip", data);
        }

    }
    console.log(`Done scene: ` + saveScene)
    fs.writeFileSync(saveScene, JSON.stringify(dataScene, null, 2));
    dataScene = []; // cleanup

    const saveStage = lang + `stage.json`;
    const getStage = readJsonFile('../Resources/ExcelOutput/StageConfig.json');
    for (const data of Object.values(getStage)) {

        if (data && data.StageName && data.StageName.Hash) {
            const hash = data.StageName.Hash;
            const id = data.StageID;

            const name = getHashLANG[hash] || getHashCN[hash] || `N/A`;

            var objScene = new Object();
            objScene["id"] = id;
            objScene["name"] = `${name} (${data.StageType} #${data.Level})`;
            dataScene.push(objScene)

        } else {
            console.log("skip", data);
        }

    }
    console.log(`Done stage: ` + saveStage)
    fs.writeFileSync(saveStage, JSON.stringify(dataScene, null, 2));
    dataScene = []; // cleanup
}
async function runMonster(lang, getHashLANG, dlicon = false) {
    const saveMonster = lang + 'monster.json';
    const getMonster = readJsonFile('../Resources/ExcelOutput/MonsterTemplateConfig.json');
    var dataMonster = [];
    for (const data of Object.values(getMonster)) {

        if (data && data.MonsterName && data.MonsterName.Hash) {
            const hash = data.MonsterName.Hash;
            const id = data.MonsterTemplateID;
            const rank = data.Rank

            const iconUrl = data.RoundIconPath
            const nameIcon = path.basename(iconUrl);

            const name = getHashLANG[hash] || getHashCN[hash] || `N/A`;

            var iconPath = `Monster_${id}`
            var urlIcon = ""

            if (dlicon) {
                if (iconPath != "") {
                    //var iconUrl = `https://enka.network/ui/hsr/${iconUrlx}`;
                    try {
                        urlIcon = await downloadIcon(
                            `../Tool/dump/spriteoutput/monsterroundicon/${nameIcon}`,
                            `../Tool/icon/monster/${nameIcon}`,
                            `/image/game/starrails/monster/${nameIcon}`);
                    } catch (error) {
                        console.log(error);
                    }
                } else {
                    // console.log(`not found iocn ${id} with name ${name}`)
                }
            }

            var objMonster = new Object();
            objMonster["id"] = id;
            objMonster["name"] = name;
            objMonster["type"] = rank;
            objMonster["img"] = urlIcon;
            dataMonster.push(objMonster)

        } else {
            console.log("skip", data);
        }

    }
    fs.writeFileSync(saveMonster, JSON.stringify(dataMonster, null, 2));
    dataMonster = []; // cleanup
    console.log(`done monster: ` + saveMonster)
}
function runProp(lang, getHashLANG) {
    const saveProp = lang + 'prop.json';
    const getProp = readJsonFile('../Resources/ExcelOutput/MazeProp.json');
    var dataProp = [];
    for (const data of Object.values(getProp)) {

        if (data && data.PropName && data.PropName.Hash) {
            const hash = data.PropName.Hash;
            const id = data.ID;
            const NamePath = data.ConfigEntityPath.replace(/^.*\//, '').replace(/\.json$/, '');

            const name = (getHashLANG[hash] || getHashCN[hash] || `N/A`) + ` (${NamePath})`;

            var objProp = new Object();
            objProp["id"] = id;
            objProp["name"] = name;
            dataProp.push(objProp)

        } else {
            console.log("skip", data);
        }

    }
    fs.writeFileSync(saveProp, JSON.stringify(dataProp, null, 2));
    dataProp = []; // cleanup
    console.log(`done Prop: ` + saveProp)
}

function runMission(lang, getHashLANG) {
    const saveMission = lang + 'mission.json';

    // we need this for get hash
    const getMainMission = readJsonFile('../Resources/ExcelOutput/MainMission.json');
    const getSubMission = readJsonFile('../Resources/ExcelOutput/SubMission.json');

    const missionPath = path.resolve('../Resources/Config/Level/Mission/');
    const missionData = fs.readdirSync(missionPath);

    var dataMission = [];
    for (const file0 of missionData) {
        //console.log(file)
        const missionPath2 = path.join(missionPath, file0);
        const missionData2 = fs.readdirSync(missionPath2);
        for (const file2 of missionData2) {
            //console.log(file2)
            if (file2.includes('MissionInfo_')) {
                const missionPath3 = path.join(missionPath2, file2);
                const dataMissionMain = readJsonFile(missionPath3);
                //console.log(dataMissionMain)
                //process.exit(1)

                var id0 = dataMissionMain.MainMissionID;
                var configMain = getMainMission.find(item => item.MainMissionID === id0);
                if (!configMain) {
                    // idk why but maybe some mission in dev or maybe hide?
                    console.log(`skip ${id0} ${file2}`)
                    continue
                }

                const hashMain = configMain.Name.Hash;
                const nameMain = (getHashLANG[hashMain] || getHashCN[hashMain] || `N/A ${hashMain}`);

                var objMain = new Object();
                objMain["id"] = id0;
                objMain["type"] = configMain.Type;
                objMain["name"] = nameMain;
                objMain["sub"] = [];
                //console.log(objMain)
                //dataMission.push(objMain)

                //console.log(dataMissionMain)
                //process.exit();

                for (const dataRaw of dataMissionMain.SubMissionList) {

                    var id1 = dataRaw.ID;
                    var configSub = getSubMission.find(item => item.SubMissionID === id1);
                    if (!configSub) {
                        // idk why but maybe some mission in dev or maybe hide?
                        console.log(`skip sub ${id1}`)
                        continue
                    }

                    const hashSub = configSub.TargetText.Hash;
                    const nameSub = (getHashLANG[hashSub] || getHashCN[hashSub] || `N/A ${hashSub}`);

                    var objSub = new Object();
                    objSub["id"] = id1;
                    objSub["name"] = nameSub;

                    objMain["sub"].push(objSub)
                }

                //console.log(objMain)

                //process.exit()

                dataMission.push(objMain)



            }
        }
    }

    fs.writeFileSync(saveMission, JSON.stringify(dataMission, null, 2));
    dataMission = []; // cleanup
    console.log(`done mission: ` + saveMission)


}
async function runItem(lang, getHashLANG, dlicon = false) {

    const saveItem = lang + 'item.json';
    var dataItem = [];

    const filePaths = [
        '../Resources/ExcelOutput/ItemConfig.json',
        '../Resources/ExcelOutput/ItemConfigAvatar.json',
        '../Resources/ExcelOutput/ItemConfigAvatarPlayerIcon.json',
        '../Resources/ExcelOutput/ItemConfigAvatarRank.json',
        '../Resources/ExcelOutput/ItemConfigBook.json',
        '../Resources/ExcelOutput/ItemConfigDisk.json',
        '../Resources/ExcelOutput/ItemPlayerCard.json'
    ];

    for (const filePath of filePaths) {
        const getItem = readJsonFile(filePath);
        for (const data of Object.values(getItem)) {
            if (data && data.ItemName && data.ItemName.Hash) {
                const hash = data.ItemName.Hash;
                const id = data.ID;

                const name = getHashLANG[hash] || getHashCN[hash] || `N/A`;

                var subType = data.ItemSubType;
                var iconUrlx = data.ItemIconPath;

                var tu = (path.dirname(iconUrlx)).toLocaleLowerCase();
                const fileName = path.basename(iconUrlx);

                var urlIcon = ``

                if (dlicon) {
                    if (iconUrlx != "") {
                        //var iconUrl = `https://api.yatta.top/hsr/assets/UI/item/${urlIcon}`;
                        try {
                            urlIcon = await downloadIcon(
                                `../Tool/dump/${tu}/${fileName}`,
                                `../Tool/icon/item/${fileName}`,
                                `/image/game/starrails/item/${fileName}`
                            );
                        } catch (error) {
                            console.log(error);
                        }
                    } else {
                        // console.log(`not found iocn ${id} with name ${name}`)
                    }
                }

                var objItem = new Object();
                objItem["id"] = id;
                objItem["name"] = `${name}`;
                objItem["type_main"] = data.ItemMainType;
                objItem["type_sub"] = subType;
                objItem["rank"] = data.Rarity;
                objItem["img"] = urlIcon

                dataItem.push(objItem);

            } else {
                console.log("skip", data);
            }
        }
    }

    fs.writeFileSync(saveItem, JSON.stringify(dataItem, null, 2));
    dataItem = []; // cleanup
    console.log(`done item: ` + saveItem)
}

async function runWeapon(lang, getHashLANG, dlicon = false) {
    const saveItem = lang + 'weapon.json';
    var dataItem = [];

    const filePaths = [
        '../Resources/ExcelOutput/ItemConfigEquipment.json' // light cone
    ];

    for (let i = 0; i < filePaths.length; i++) {
        const filePath = filePaths[i];
        const getItem = readJsonFile(filePath);
        for (const data of Object.values(getItem)) {
            if (data && data.ItemName && data.ItemName.Hash) {
                const hash = data.ItemName.Hash;
                const id = data.ID;

                const name = getHashLANG[hash] || getHashCN[hash] || `N/A`;

                var subType = data.ItemSubType;
                var iconUrlx = data.ItemIconPath;

                var tu = (path.dirname(iconUrlx)).toLocaleLowerCase();
                const fileName = path.basename(iconUrlx);

                var urlIcon = ``
                if (dlicon) {
                    if (iconUrlx != "") {
                        try {
                            urlIcon = await downloadIcon(
                                `../Tool/dump/${tu}/${fileName}`,
                                `../Tool/icon/lightcone/${fileName}`,
                                `/image/game/starrails/lightcone/${fileName}`
                            );
                        } catch (error) {
                            console.log(error);
                        }
                    } else {
                        // console.log(`not found iocn ${id} with name ${name}`)
                    }
                }

                var objItem = new Object();
                objItem["id"] = id;
                objItem["name"] = `${name}`;
                objItem["type_main"] = data.ItemMainType;
                objItem["type_sub"] = subType;
                objItem["rank"] = data.Rarity;
                objItem["img"] = urlIcon

                dataItem.push(objItem);

            } else {
                console.log("skip", data);
            }
        }
    }

    fs.writeFileSync(saveItem, JSON.stringify(dataItem, null, 2));
    dataItem = []; // cleanup
    console.log(`done wp: ` + saveItem);
}

const getRelicConfig = readJsonFile('../Resources/ExcelOutput/RelicConfig.json');
const getRelicMain = readJsonFile('../Resources/ExcelOutput/RelicMainAffixConfig.json');
const getRelicSub = readJsonFile('../Resources/ExcelOutput/RelicSubAffixConfig.json');
const getAvatarProp = readJsonFile('../Resources/ExcelOutput/AvatarPropertyConfig.json');

// Simulator 
// /give 61162 4:1 5:4 7:3 12:1 lv15
// /give [relic id] s[main stat id] [sub statid]:[count]
function runGiveEmu(cmd) {

    const parts = cmd.split(" ");
    const action = parts[1];

    const item_id = parseInt(action);

    let level = 0;
    let item_count = 1;
    let refined_count = 0;
    let main_prop_id = 0;
    let propDataCount = 0;
    var maxStep = false;
    var maxLevel = 900;

    if (parts.length > 2) {
        let currentParamIndex = 2;
        while (currentParamIndex < parts.length) {

            const param = parts[currentParamIndex];
            //console.log(param)

            if (param.startsWith("x")) {
                item_count = parseInt(param.replace("x", ""));
            } else if (param.startsWith("s")) {
                main_prop_id = parseInt(param.replace("s", ""));
            } else if (param.startsWith("lv")) {
                level = parseInt(param.replace("lv", ""));
                //console.log(`found level: `+level)
            } else if (param.startsWith("r")) {
                refined_count = parseInt(param.replace("r", "")) + 1;
            } else if (param.startsWith("sorted")) {
                //continue
            } else if (param.startsWith("-maxsteps")) {
                maxStep = true;
                //console.log(`found max`)
            } else {

                var dataRelic = getRelicConfig[item_id];
                //console.log(dataRelic)

                // main prop cannot be more than 5 and less than 1
                if (main_prop_id > 5) {
                    main_prop_id = 5;
                }
                if (main_prop_id <= 0) {
                    main_prop_id = 1;
                }

                var dataRelicMain = getRelicMain[dataRelic.MainAffixGroup][main_prop_id]
                if (dataRelicMain) {

                    //console.log(dataRelicMain)
                    //console.log(`RelicMainAffixConfig ${dataRelic.MainAffixGroup} > ${main_prop_id}`)

                    var mainPropHash = getAvatarProp[dataRelicMain.Property].PropertyName.Hash
                    const nameMain = getHashEN[mainPropHash] || getHashCN[mainPropHash] || `N/A`;

                    if (dataRelicMain.Property.endsWith('Delta')) {
                        console.log(`Main ${nameMain}: ${Math.floor(dataRelicMain.BaseValue.Value + (dataRelicMain.LevelAdd.Value * level))}`)
                    } else {
                        console.log(`Main ${nameMain}: ${((dataRelicMain.BaseValue.Value + (dataRelicMain.LevelAdd.Value * level)) * 100).toFixed(2)} %`)
                    }

                    // Add sub prop
                    for (let i = currentParamIndex; i < parts.length; i++) {
                        const propData = parts[i].split(/[,:;.]/);
                        const prop_id = parseInt(propData[0]);
                        let prop_count = parseInt(propData[1]);

                        var SubData = getRelicSub[dataRelic.SubAffixGroup][prop_id]
                        if (!SubData) continue
                        //console.log(SubData)

                        // limit count base level max art
                        const maxCount = Math.floor(maxLevel / 3) + 1; // or just use 15 ?
                        const count = Math.min(prop_count, maxCount);

                        // fix step count
                        var finalValue = SubData.BaseValue.Value + (SubData.StepValue.Value * 2);
                        if (count >= 2) {
                            finalValue = finalValue * count
                        }

                        var subPropHash = getAvatarProp[SubData.Property].PropertyName.Hash
                        const nameSub = getHashEN[subPropHash] || getHashCN[subPropHash] || `N/A`;

                        if (SubData.Property.endsWith('Delta')) {
                            console.log(`Sub ${nameSub}: ${Math.floor(finalValue)}`)
                        } else {
                            console.log(`Sub ${nameSub}: ${(finalValue * 100).toFixed(2)} %`)
                        }

                        propDataCount++;
                    }
                } else {
                    console.log(`not found`)
                }

                break;
            }
            currentParamIndex++;
        }
    } else {
        return console.log(`bad cmd`)
    }

    //console.log(extra_params)

    console.log(`${item_id} x${propDataCount} lv${level}`)
}
async function runRelic(lang, getHashLANG, dlicon = false) {

    // lock basic stats
    var maxLevel = 0;
    var maxStep = 2;

    const saveMainRelic = lang + 'relic_main.json';
    var dataMainRelic = [];
    for (const data of Object.values(getRelicMain)) {

        var grup = data.GroupID;
        var id = data.AffixID;
        var name = data.Property;

        var mainPropHash = `N/A`
        try {
            mainPropHash = getAvatarProp.find(item => item.PropertyType === name).PropertyName.Hash;
        } catch (error) {
            console.log(name)
        }
        const nameMain = getHashLANG[mainPropHash] || getHashCN[mainPropHash] || `N/A`;

        var bonus = ``;
        if (name.endsWith('Delta')) {
            bonus = `${Math.floor(data.BaseValue.Value + (data.LevelAdd.Value * maxLevel))}`
        } else {
            bonus = `${((data.BaseValue.Value + (data.LevelAdd.Value * maxLevel)) * 100).toFixed(2)} %`
        }

        var objMainRelic = new Object();
        objMainRelic["id"] = id;
        objMainRelic["name"] = `${nameMain} (${bonus})`;
        objMainRelic["grup"] = grup;
        dataMainRelic.push(objMainRelic)

    }
    fs.writeFileSync(saveMainRelic, JSON.stringify(dataMainRelic, null, 2));
    dataMainRelic = []; // cleanup
    console.log(`done MainRelic: ` + saveMainRelic)

    const saveSubRelic = lang + 'relic_sub.json';
    var dataSubRelic = [];
    for (const data of Object.values(getRelicSub)) {
        var grup = data.GroupID;
        var id = data.AffixID;
        var name = data.Property;

        var mainPropHash = `N/A`
        try {
            mainPropHash = getAvatarProp.find(item => item.PropertyType === name).PropertyName.Hash;
        } catch (error) {
            console.log(name)
        }
        const nameMain = getHashLANG[mainPropHash] || getHashCN[mainPropHash] || `N/A`;

        var bonus = ``;
        var finalValue = data.BaseValue.Value + (data.StepValue.Value * maxStep);
        if (name.endsWith('Delta')) {
            bonus = `${Math.floor(finalValue)}`
        } else {
            bonus = `${(finalValue * 100).toFixed(2)} %`
        }

        var objSubRelic = new Object();
        objSubRelic["id"] = id;
        objSubRelic["name"] = `${nameMain} (${bonus})`;
        objSubRelic["grup"] = grup;
        dataSubRelic.push(objSubRelic)
    }
    fs.writeFileSync(saveSubRelic, JSON.stringify(dataSubRelic, null, 2));
    dataSubRelic = []; // cleanup
    console.log(`done SubRelic: ` + saveSubRelic)

    const saveItemRelic = lang + 'relic_item.json';
    const getItemRelic = readJsonFile('../Resources/ExcelOutput/ItemConfigRelic.json');
    var dataItemRelic = [];
    for (const data of Object.values(getItemRelic)) {
        const hash = data.ItemName.Hash;
        const id = data.ID;

        var config = getRelicConfig.find(item => item.ID === id);

        const name = getHashLANG[hash] || getHashCN[hash] || `N/A`;

        var iconUrlx = data.ItemIconPath;

        var tu = (path.dirname(iconUrlx)).toLocaleLowerCase();
        const fileName = path.basename(iconUrlx);

        var urlIcon = ``
        if (dlicon) {
            if (iconUrlx != "") {
                try {
                    urlIcon = await downloadIcon(
                        `../Tool/dump/${tu}/${fileName}`,
                        `../Tool/icon/relic/${fileName}`,
                        `/image/game/starrails/relic/${fileName}`
                    );
                } catch (error) {
                    console.log(error);
                }
            } else {
                // console.log(`not found iocn ${id} with name ${name}`)
            }
        }

        var objItem = new Object();
        objItem["id"] = id;
        objItem["name"] = `${name}`;
        objItem["rank"] = data.Rarity;
        objItem["main"] = config.MainAffixGroup;
        objItem["sub"] = config.SubAffixGroup;
        objItem["type"] = config.Type;
        objItem["img"] = urlIcon
        dataItemRelic.push(objItem)
    }
    fs.writeFileSync(saveItemRelic, JSON.stringify(dataItemRelic, null, 2));
    dataItemRelic = []; // cleanup
    console.log(`done Item Relic: ` + saveItemRelic)
}

async function runMappings(version = 1) {
    var dataMappings = {};
    const saveMappings = '../Tool/data/mappings.json';

    for (const textMapFile of textMapFiles) {

        if (textMapFile.includes(`Main`)) {
            continue;
        }

        var lang = textMapFile.replace("TextMap", "").replace(".json", "");

        if (version == 1) {
            const textMapFilePath = path.resolve(textMapFolderPath, textMapFile);
            const textMapData = await readJsonFileAsync(textMapFilePath);


            // Initialize the object if it's undefined
            if (!dataMappings[mapLanguageCode(lang, true)]) {
                dataMappings[mapLanguageCode(lang, true)] = {};
            }

            const filePaths = [
                '../Resources/ExcelOutput/ItemConfigAvatar.json',
                '../Resources/ExcelOutput/ItemConfigEquipment.json'
            ];

            for (const filePath of filePaths) {
                try {
                    const getAvatar = await readJsonFileAsync(filePath);
                    for (const data of Object.values(getAvatar)) {
                        if (data && data.ItemName && data.ItemName.Hash) {
                            const hash = data.ItemName.Hash;
                            const id = data.ID;
                            const rank = getColorByRankLevel(data.Rarity);
                            const name = textMapData[hash] || `N/A`;

                            var nameType = "idk";
                            if (filePath.includes(`ItemConfigAvatar`)) {
                                nameType = textMapData[`465326605`] || `N/A`;
                            } else if (filePath.includes(`ItemConfigEquipment`)) {
                                nameType = textMapData[`1185710709`] || `N/A`;
                            }

                            dataMappings[mapLanguageCode(lang, true)][id] = [`${name} (${nameType})`, rank];
                        } else {
                            console.log("skip", data);
                        }
                    }
                } catch (error) {
                    console.error("Error processing file:", error);
                }
            }

            // Type Banner
            dataMappings[mapLanguageCode(lang, true)][1] = textMapData[`1027493017`] || `N/A`; // Newbie
            dataMappings[mapLanguageCode(lang, true)][2] = textMapData[`684974156`] || `N/A`; // Normal
            dataMappings[mapLanguageCode(lang, true)][11] = textMapData[`910428004`] || `N/A`; // AvatarUp
            dataMappings[mapLanguageCode(lang, true)][12] = textMapData[`-641761534`] || `N/A`; // WeaponUp
        } else {

            const langDirectory = `../Tool/resources/${mapLanguageCode(lang, false)}/`;

            if (!fs.existsSync(langDirectory)) {
                fs.mkdirSync(langDirectory, { recursive: true });
            }

            //console.log(`>> ${lang}`)

           const getHashLANG = readJsonFile(`../Resources/TextMap/TextMap${lang}.json`);

            //await runAvatar(langDirectory, getHashLANG, true)
            //runScene(langDirectory, getHashLANG)
            //await runMonster(langDirectory, getHashLANG, true)
            //await runItem(langDirectory, getHashLANG, true)
            //await runRelic(langDirectory, getHashLANG, true)
            //await runWeapon(langDirectory, getHashLANG, true)
            //runProp(langDirectory, getHashLANG)
            runMission(langDirectory, getHashLANG)
        }

    }

    if (version == 1) {
        await writeFileAsync(saveMappings, JSON.stringify(dataMappings, null, 2), 'utf-8');
    }
}

//runAvatar();
//runScene();
//runMonster();
//runItem();
//runRelic();
//runWeapon();
//runProp();
runMappings(2);
//runGiveEmu(`/give 63036 s1 lv1 1:1 2:1 3:1 4:1`);
//runGiveEmu(`/give 63036 lv15 s1 1:99 2:1 3:1 4:1`);
//runGiveEmu(`/give 63116 lv15 s4 4:1 7:2 8:1 9:5`);
//runGiveEmu(`/give 63115 lv15 s10 5:1 7:1 8:1 9:6`);
//runGiveEmu(`/give 61013 lv20 s1 1:10000000 5:100000000 7:100000 10:100000`)

// Function to read and parse JSON file
function readJsonFile(filePath) {
    const fileContent = fs.readFileSync(filePath, 'utf-8');
    return JSON.parse(fileContent);
}

async function readJsonFileAsync(filePath) {
    try {
        const fileContent = await new Promise((resolve, reject) => {
            fs.readFile(filePath, 'utf-8', (err, data) => {
                if (err) reject(err);
                else resolve(data);
            });
        });
        return JSON.parse(fileContent);
    } catch (error) {
        console.error(`Error reading JSON file at ${filePath}:`, error);
        throw error;
    }
}

async function writeFileAsync(filePath, content, encoding = 'utf-8') {
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, content, encoding, (err) => {
            if (err) reject(err);
            else resolve();
        });
    });
}

function mapLanguageCode(languageCode, useLowerCase = false) {
    // http://www.lingoes.net/en/translator/langcode.htm
    const languageMap = {
        CHS: 'zh_CN',
        CHT: 'zh_TW',
        DE: 'de_DE', // TODO
        EN: 'en_US',
        ES: 'es_ES',
        FR: 'fr_FR',
        ID: 'id_ID',
        JP: 'ja_JP',
        KR: 'ko_KR',
        PT: 'pt_PT', // TODO
        RU: 'ru_RU',
        TH: 'th_TH',
        VI: 'vi_VN',
        IT: 'it_IT', // TODO
        TR: `tr_TR` // TODO
    };
    // Check if the language code has a mapping, otherwise use the original code
    var tus = languageMap[languageCode] || languageCode;
    if (useLowerCase) {
        tus = tus.toLowerCase();
    }
    return tus;
}

function getColorByRankLevel(rankLevel) {
    switch (rankLevel) {
        case "Rare":
            return "blue";
        case "VeryRare":
            return "purple";
        case "SuperRare":
            return "yellow";
        default:
            return "";
    }
}

async function downloadIcon(url, destination, localpb, urlif404 = "", timeout = 7000) {
    if (await fileExists(destination)) {
        // Check file size
        const stats = await fs.promises.stat(destination);
        if (stats.size === 0) {
            localpb = ""; // Set localpb to empty if file size is 0KB
            //console.log(`File exists but has size 0KB: ${destination}`);
        } else {
            //console.log(`File already exists at ${destination}. Skipping download.`);            
        }
        return localpb;
    }

    console.log(`File not found ${destination}, downloading from ${url}`);

    if (!isValidUrl(url)) {
        try {
            // Correcting the fs.copyFile call with callback function
            await fs.copyFile(url, destination, (err) => {
                if (err) {
                    return "";
                }
            });
            return localpb;
        } catch (error) {
            console.error(`Failed to copy local file ${url} to ${destination}: ${error.message}`);
            // Proceed with fallback logic or handle error
            return ""
        }
    } else {

        const fileStream = fs.createWriteStream(destination);

        return new Promise((resolve, reject) => {
            const request = https.get(url, response => {
                if (response.statusCode === 200) {
                    response.pipe(fileStream);
                    fileStream.on('finish', () => {
                        fileStream.close();
                        resolve(localpb);
                    });
                } else if (urlif404 && urlif404 !== "") {
                    console.log(`Failed to download icon from ${url} (${response.statusCode}). Trying alternative URL: ${urlif404}`);
                    fileStream.close();
                    fs.unlink(destination, async () => {
                        try {
                            await downloadIcon(urlif404, destination, localpb);
                            resolve(localpb);
                        } catch (error) {
                            reject(error);
                        }
                    });
                } else {
                    fileStream.close();
                    //fs.unlink(destination, () => {}); // Remove partially downloaded file
                    reject(`Failed to download icon ${url} (${response.statusCode})`);
                }
            }).on('error', async err => {
                fileStream.close();
                fs.unlink(destination, async () => {
                    try {
                        await downloadIcon(urlif404, destination, localpb);
                        resolve(localpb);
                    } catch (error) {
                        reject(error);
                    }
                });
            });

            // Set a timeout for the request
            request.setTimeout(timeout, () => {
                request.destroy(); // Abort the request if it exceeds the timeout
                fileStream.close();
                fs.unlink(destination, () => { }); // Remove partially downloaded file
                reject(new Error(`Request timed out after ${timeout} milliseconds`));
            });
        });
    }
}

async function fileExists(filePath) {
    return new Promise((resolve, reject) => {
        fs.access(filePath, fs.constants.F_OK, (err) => {
            if (err) {
                if (err.code === 'ENOENT') {
                    resolve(false); // File does not exist
                } else {
                    reject(err); // Other errors
                }
            } else {
                resolve(true); // File exists
            }
        });
    });
}

// Function to check if a string is a valid URL
function isValidUrl(string) {
    try {
        new URL(string);
        return true;
    } catch (error) {
        return false;
    }
}